/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ab.entity.HelloPerson;

@RestController

public class SimpleRestController {

	@Autowired
	private HelloPerson helloPerson;
	
	
	/**
	 * @param name
	 * @return
	 */
	@RequestMapping(value="/hello")
	public String hello(@RequestParam("name") String name){
		return "Hello, Your accessing my first rest sevice.Thank you - "+name+" \n"+helloPerson.getGreeting();
	}
	
}
