/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.ab.assembler.CustomerModeEntityAssembler;
import com.ab.entity.Address;
import com.ab.entity.City;
import com.ab.entity.Customer;
import com.ab.exception.CustomerCreationException;
import com.ab.exception.CustomerDeletionException;
import com.ab.exception.CustomerLoadException;
import com.ab.model.CustomerModel;
import com.ab.service.CityService;
import com.ab.service.CustomerService;

@Controller
public class WebController {

	Logger logger = LoggerFactory.getLogger(WebController.class);
	
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private CityService cityService;
	
	@RequestMapping(value="/welcome")
	public String welcome(){
		return "welcome";
	}
	
	
	@RequestMapping(value="/home")
	public String home(Map<String,Object> model){
		
		logger.info("Fetching Customer Creation page");
		
		CustomerModel customer = new CustomerModel();
		
		List<Address> list=new ArrayList<>();
		
		list.add(new Address());
		
		list.add(new Address());
		
		customer.setListOfAddress(list);
		
		model.put("customer", customer);
		model.put("msg", "Create a new customer");
		
		model.put("cityList", cityService.getCityList());
		
		logger.info("Redirecting to the page");
		
		return "index";
	}
	
	
	@RequestMapping(value="/saveCustomer",method=RequestMethod.POST)
	public String saveCustomer(@ModelAttribute("customer") CustomerModel customerModel,Map<String,Object> model) throws CustomerDeletionException, CustomerCreationException, CustomerLoadException{
		
		
		Customer customer = CustomerModeEntityAssembler.populateEntityFromModel(customerModel);
		
		Customer customerDb=customerService.findByFirstNameAndLastName(customer.getFirstName(),customer.getLastName());
		
		if(customerDb != null && customer.getId()==null){
			
			home(model);
			
			model.put("customer",customer);
			
			logger.info("Duplicate Customer");
			
			model.put("msg2", "Customer With Same First Name and Last Name Already Exist");
			
			return "index";
		}
		
		
		logger.info("Saving Customer Details...");
		
		customerService.saveCustomer(customer);
		
		model.put("customerList", customerService.findAllCustomer());
		
		
		logger.info("Customer Details are saved... Fetching the list of customer....");
		
		return "customerList";
	}
	
	@RequestMapping(value="/customerList",method=RequestMethod.GET)
	public String customerList(Map<String,Object> model) throws CustomerLoadException{
		
		logger.info("Fethcing the List of customer......");
		
		model.put("customerList", customerService.findAllCustomer());
		
		return "customerList";
	}
	
	@RequestMapping(value="/deleteCustomer",method=RequestMethod.GET)
	public String deleteCustomer(@RequestParam("customerId")String customerId,Map<String,Object> model) throws CustomerDeletionException, CustomerLoadException{
		
		logger.info("Deleting the customer......"+customerId);
		
		customerService.deleteCustomerById(customerId);
		
		model.put("customerList", customerService.findAllCustomer());
		
		return "customerList";
	}
	
	@RequestMapping(value="/editCustomer",method=RequestMethod.GET)
	public String editCustomer(@RequestParam("customerId")String customerId,Map<String,Object> model) throws CustomerLoadException{
		
		logger.info("Fetching customer details for......"+customerId);
		
		Customer customer=customerService.findCustomerById(customerId);
		
		CustomerModel customerModel = CustomerModeEntityAssembler.populateModelFromEntity(customer);
		
		home(model);
		
		model.put("customer", customerModel);
		
		return "index";
	}
	
	@RequestMapping(value="/{custId}/customerDetails",method=RequestMethod.GET)
	public String customerDetails(@PathVariable("custId")String custId,Map<String,Object> model) throws CustomerLoadException{
		logger.info("Fetching customer details for......"+custId);
		
		Customer customer=customerService.findCustomerById(custId);
		
		model.put("customer", customer);
		
		List<City> listOfCities = cityService.getCityList();
		
		Map<String,String> mappedCities = listOfCities.stream().collect(Collectors.toMap(City::getCityId, (city) -> city.getCityName()));
		
		model.put("cityMap", mappedCities);
		
		logger.info("Redirecting to edit page");
		
		return "customerDetails";
	}
	
	@ExceptionHandler(CustomerCreationException.class)
	public String handleException(CustomerCreationException e,Map<String,Object> model){
		logger.error(e.getMessage(), e);
		model.put("msg", e.getMessage());
		return "error";
	}
	
	@ExceptionHandler(CustomerLoadException.class)
	public String handleException(CustomerLoadException e,Map<String,Object> model){
		logger.error(e.getMessage(), e);
		model.put("msg", e.getMessage());
		return "error";
	}
	
	@ExceptionHandler(CustomerDeletionException.class)
	public String handleException(CustomerDeletionException e,Map<String,Object> model){
		logger.error(e.getMessage(), e);
		model.put("msg", e.getMessage());
		return "error";
	}
}
