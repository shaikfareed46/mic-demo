/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.entity;

public class UserLog {

	private String userName;
	
	private String password;
	
	private String userIpAddress;
	
	private int connectionStatus;
	
	private String description;

	/**
	 * @return
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return
	 */
	public String getUserIpAddress() {
		return userIpAddress;
	}

	/**
	 * @param userIpAddress
	 */
	public void setUserIpAddress(String userIpAddress) {
		this.userIpAddress = userIpAddress;
	}

	/**
	 * @return
	 */
	public int getConnectionStatus() {
		return connectionStatus;
	}

	/**
	 * @param connectionStatus
	 */
	public void setConnectionStatus(int connectionStatus) {
		this.connectionStatus = connectionStatus;
	}

	/**
	 * @return
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
}
