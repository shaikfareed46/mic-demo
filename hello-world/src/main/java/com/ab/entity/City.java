/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="City")
public class City {
	
	@Id
	private String cityId;
	
	private String cityName;

	
	/**
	 * @return
	 */
	public String getCityId() {
		return cityId;
	}

	/**
	 * @param cityId
	 */
	public void setCityId(String cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return
	 */
	public String getCityName() {
		return cityName;
	}

	/**
	 * @param cityName
	 */
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	
}
