/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.exception;

/*
 *  * Copyrights of Accurate background 
 *  
 *  
 * 
*/
public class CustomerCreationException extends Exception {

	/**
	 * This exception is thrown when there is problem saving customer
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public CustomerCreationException(){
		super("Customer could not be saved:");
	}

	/**
	 * @param msg
	 */
	public CustomerCreationException(String msg){
		super(msg);
	}
	
	/**
	 * @param msg
	 * @param t
	 */
	public CustomerCreationException(String msg,Throwable t){
		super(msg,t);
	}
}
