/**
 * 
 * Copyright of Accurate Background 
 * 
 * Author: Shaik Fareed
 * 
 * Team : Microservices
 */
package com.ab.exception;

public class CustomerLoadException extends Exception {


	/**
	 * This exception is thrown when there is problem fetching customer
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public CustomerLoadException(){
		super("Could not fetch customer list:");
	}

	/**
	 * @param msg
	 */
	public CustomerLoadException(String msg){
		super(msg);
	}
	
	/**
	 * @param msg
	 * @param t
	 */
	public CustomerLoadException(String msg,Throwable t){
		super(msg,t);
	}

}
