/**
 * 
 * Copyright of Accurate Background 
 * 
 * Author: Shaik Fareed
 * 
 * Team : Microservices
 */
package com.ab.exception;

public class CustomerDeletionException extends Exception {
	/**
	 * This exception is thrown when there is problem fetching customer
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public CustomerDeletionException(){
		super("Could not delete customer");
	}

	/**
	 * @param msg
	 */
	public CustomerDeletionException(String msg){
		super(msg);
	}
	
	/**
	 * @param msg
	 * @param t
	 */
	public CustomerDeletionException(String msg,Throwable t){
		super(msg,t);
	}

}
