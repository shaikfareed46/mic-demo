/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelloWorldApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelloWorldApplication.class, args);
    }
    
    
}
