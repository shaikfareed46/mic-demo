/**
 * 
 * Copyright of Accurate Background 
 * 
 * Author: Shaik Fareed
 * 
 * Team : Microservices 
*
 */
package com.ab.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ab.entity.Customer;
import com.ab.exception.CustomerCreationException;
import com.ab.exception.CustomerDeletionException;
import com.ab.exception.CustomerLoadException;
import com.ab.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepo;
	
	
	/* (non-Javadoc)
	 * @see com.ab.service.CustomerService#saveCustomer(com.ab.entity.Customer)
	 */
	@Override
	public void saveCustomer(Customer customer) throws CustomerCreationException {
		try{
			customerRepo.save(customer);
		}
		catch(Exception ex){
			throw new CustomerCreationException(ex.getMessage(),ex);
		}
	}

	/* (non-Javadoc)
	 * @see com.ab.service.CustomerService#findAllCustomer()
	 */
	@Override
	public List<Customer> findAllCustomer() throws CustomerLoadException {
		try{
			return customerRepo.findAll();
		}catch(Exception ex){
			throw new CustomerLoadException(ex.getMessage(),ex);
		}

	}

	/* (non-Javadoc)
	 * @see com.ab.service.CustomerService#deleteCustomerById(java.lang.String)
	 */
	@Override
	public void deleteCustomerById(String customerId) throws CustomerDeletionException{
		try{
			customerRepo.delete(customerId);
		}catch(Exception ex){
			throw new CustomerDeletionException(ex.getMessage(),ex);
		}
	}

	/* (non-Javadoc)
	 * @see com.ab.service.CustomerService#findByFirstNameAndLastName(java.lang.String, java.lang.String)
	 */
	@Override
	public Customer findByFirstNameAndLastName(String firstName, String lastName) throws CustomerLoadException {
		try{
			return customerRepo.findByFirstNameAndLastName(firstName,lastName);
		}catch(Exception ex){
			throw new CustomerLoadException(ex.getMessage(),ex);
		}
	}

	/* (non-Javadoc)
	 * @see com.ab.service.CustomerService#findCustomerById(java.lang.String)
	 */
	@Override
	public Customer findCustomerById(String customerId)  throws CustomerLoadException {
		try{
			return customerRepo.findOne(customerId);
		}catch(Exception ex){
			throw new CustomerLoadException(ex.getMessage(),ex);
		}
	}


}
