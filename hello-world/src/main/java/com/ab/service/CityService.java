/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.service;

import java.util.List;

import com.ab.entity.City;

public interface CityService {
	
	/**
	 * @return
	 */
	List<City> getCityList();
	
}
