/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ab.entity.City;
import com.ab.repository.CityRepository;

@Service
public class CityServiceImpl implements CityService{

	@Autowired
	private CityRepository cityRepo;
	
	/* (non-Javadoc)
	 * @see com.ab.service.CityService#getCityList()
	 */
	@Override
	public List<City> getCityList() {
		return cityRepo.findAll();
	}
	
}
