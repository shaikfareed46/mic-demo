/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.service;

import java.util.List;

import com.ab.entity.Customer;
import com.ab.exception.CustomerCreationException;
import com.ab.exception.CustomerDeletionException;
import com.ab.exception.CustomerLoadException;

public interface CustomerService {

	/**
	 * @param customer
	 * @throws CustomerCreationException 
	 */
	void saveCustomer(Customer customer) throws CustomerCreationException;

	/**
	 * @return
	 * @throws CustomerLoadException 
	 */
	List<Customer> findAllCustomer() throws CustomerLoadException;

	/**
	 * @param customerId
	 * @throws CustomerDeletionException 
	 */
	void deleteCustomerById(String customerId) throws CustomerDeletionException;

	/**
	 * @param firstName
	 * @param lastName
	 * @return
	 * @throws CustomerLoadException 
	 */
	Customer findByFirstNameAndLastName(String firstName, String lastName) throws CustomerLoadException;

	/**
	 * @param customerId
	 * @return
	 * @throws CustomerLoadException 
	 */
	Customer findCustomerById(String customerId) throws CustomerLoadException;

	
}
