/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.config;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/*
 *  * Copyrights of Accurate background 
 *  
 *  
 * 
*/
public class RabbitMQUtil {

	public static Logger logger = LoggerFactory.getLogger(RabbitMQUtil.class);
	
	
	
	/**
	 * @return
	 * @throws IOException
	 * @throws TimeoutException
	 */
	private static Connection createConnection() throws IOException, TimeoutException{
		
		logger.info("Creating channel");
		
		ConnectionFactory connectionFactory = new ConnectionFactory();
		connectionFactory.setHost(RabbitMqConstants.HOST_IP_ADDRESS);
		connectionFactory.setVirtualHost(RabbitMqConstants.VIRTUAL_HOST);
		connectionFactory.setUsername(RabbitMqConstants.USERNAME);
		connectionFactory.setPassword(RabbitMqConstants.PASSWORD);
		connectionFactory.setPort(RabbitMqConstants.PORT);
		connectionFactory.setConnectionTimeout(50000);
		connectionFactory.setHandshakeTimeout(100000);
		Connection connection = connectionFactory.newConnection();
		
		
		logger.info("Channel created");
		
		return connection;
	}
	
	
	/**
	 * @param json
	 */
	public static void sendMessage(String json){
		Connection connection =null;
		Channel channel = null;
		try {
			connection =createConnection(); 
			channel = connection.createChannel();
			channel.queueDeclarePassive(RabbitMqConstants.QUEUE_NAME);
			logger.info("Sending message");
			channel.basicPublish("", RabbitMqConstants.QUEUE_NAME, null, json.getBytes());
			logger.info("Message Sent to server:"+json);
			logger.info("Closing channel");
		} catch (IOException  | TimeoutException e2) {
			logger.info("Connection could not be created:");
			e2.printStackTrace();
		}finally{
			try {
				if(channel!=null){
					channel.close();
				}if(connection != null){
					connection.close();
				}
				
			} catch (IOException | TimeoutException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	/**
	 * @param o
	 */
	public static void sendMessage(Object o){
		JSONObject jsonObject = new JSONObject(o);
		sendMessage(jsonObject.toString());
	}
	
}
