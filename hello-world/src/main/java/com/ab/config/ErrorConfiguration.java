/**
 * 
 * Copyright of Accurate Background 
 * 
 * Author: Shaik Fareed
 * 
 * Team : Microservices
 */
package com.ab.config;

import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.context.embedded.ErrorPage;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

@Configuration
class ErrorConfiguration implements EmbeddedServletContainerCustomizer {
   
   /**
     * Set error pages for specific error response codes
     */
   @Override public void customize( ConfigurableEmbeddedServletContainer container ) {
       container.addErrorPages( new ErrorPage( HttpStatus.NOT_FOUND, "/errors_404") ,
    		   					new ErrorPage( HttpStatus.FORBIDDEN, "/errors_403" ) , 
    		   					new ErrorPage( HttpStatus.INTERNAL_SERVER_ERROR, "/errors_500" ),
    		   					new ErrorPage( "/errorCustom" ));
   }

}

