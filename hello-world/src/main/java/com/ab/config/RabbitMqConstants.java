/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.config;

public class RabbitMqConstants {

	public static final String HOST_IP_ADDRESS = "10.34.180.57";
	
	public static final String USERNAME = "logger";
	
	public static final String PASSWORD = "Pass@123";
	
	public static final int PORT = 5672;
	
	public static final String QUEUE_NAME = "test_queue";
	
	public static final String VIRTUAL_HOST = "test_queues";
	
}
