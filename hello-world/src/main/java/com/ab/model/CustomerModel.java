/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.model;

import java.util.List;

import com.ab.entity.Address;

public class CustomerModel {

	private String id;

	private String firstName;
	
	private String lastName;
	
	private List<Address> listOfAddress;
	
	private long age;
	
	/**
	 * 
	 */
	public CustomerModel() {}

	/**
	 * @param firstName
	 * @param lastName
	 */
	public CustomerModel(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}



	/**
	 * @return
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	/**
	 * @return
	 */
	public List<Address> getListOfAddress() {
		return listOfAddress;
	}

	/**
	 * @param listOfAddress
	 */
	public void setListOfAddress(List<Address> listOfAddress) {
		this.listOfAddress = listOfAddress;
	}

	/**
	 * @return
	 */
	public long getAge() {
		return age;
	}

	/**
	 * @param age
	 */
	public void setAge(long age) {
		this.age = age;
	}

	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return String.format(
				"Customer[id=%s, firstName='%s', lastName='%s']",
				id, firstName, lastName);
	}

}

