/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.model;

public class AddressModel {

	
	private String addressLine;

	private String city;
		
	private String state;
	
	private String zip;

	/**
	 * @return
	 */
	public String getAddressLine() {
		return addressLine;
	}

	/**
	 * @param addressLine
	 */
	public void setAddressLine(String addressLine) {
		this.addressLine = addressLine;
	}

	/**
	 * @return
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	
	
	

}
