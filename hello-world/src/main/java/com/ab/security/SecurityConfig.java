/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;

@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	/*@Autowired
	private MongoDBAuthorizationProvider mongoDBAuthorizationProvider;*/
	
	
	@Autowired
	private AuthSuccessHandler authSuccessHandler;
	
	@Autowired
	private AuthFailureHandler authenticationFailureHandler;

	@Autowired
	private AccDeniedHandler accessDeniedHandler;
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	http
        .authorizeRequests().antMatchers("/home","/customerList","/saveCustomer","/editCustomer","/deleteCustomer").hasRole("ADMIN")
        .antMatchers("/customerList","/customerDetails").hasAnyRole("ADMIN","USER")
        .antMatchers("/welcome").permitAll()
        .anyRequest().authenticated()
        .and().exceptionHandling().accessDeniedHandler(accessDeniedHandler)
        .and()
        .formLogin()
        .defaultSuccessUrl("/customerList")
        .loginPage("/login").successHandler(authSuccessHandler)
        .failureHandler(authenticationFailureHandler)
        .permitAll()
        .and().logout()
        .permitAll();
    	
    	
    	
    	
    	
    	/*http.userDetailsService(mo)*/
    	http.csrf().disable();
    }

    /**
     * @param auth
     * @throws Exception
     */
    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        /*auth.authenticationProvider(mongoDBAuthorizationProvider);*/
    	
    	auth
        .inMemoryAuthentication()
            .withUser("Fareed").password("password").roles("USER");
    	
    	auth
        .inMemoryAuthentication()
            .withUser("Alex").password("password").roles("ADMIN");
    	
    }

	
	
}
