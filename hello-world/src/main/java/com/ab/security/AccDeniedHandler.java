/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import com.ab.config.RabbitMQUtil;
import com.ab.entity.UserLog;

@Component
public class AccDeniedHandler implements AccessDeniedHandler {

	/* (non-Javadoc)
	 * @see org.springframework.security.web.access.AccessDeniedHandler#handle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.access.AccessDeniedException)
	 */
	@Override
	public void handle(HttpServletRequest req, HttpServletResponse resp, AccessDeniedException accessDeniedException) throws IOException, ServletException {
		
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		
		UserLog userLog =new UserLog();
		userLog.setUserName(user.getUsername());
		userLog.setPassword(user.getPassword());
		userLog.setUserIpAddress(req.getRemoteAddr());
		userLog.setConnectionStatus(403);
		userLog.setDescription("User is not authorized to access the url");
		RabbitMQUtil.sendMessage(userLog);
		
		resp.sendError(403);
	}

}
