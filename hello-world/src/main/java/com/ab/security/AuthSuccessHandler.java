/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.ab.config.RabbitMQUtil;
import com.ab.entity.UserLog;

@Component
public class AuthSuccessHandler implements AuthenticationSuccessHandler{

	public static Logger logger = LoggerFactory.getLogger(AuthSuccessHandler.class);
	
	/* (non-Javadoc)
	 * @see org.springframework.security.web.authentication.AuthenticationSuccessHandler#onAuthenticationSuccess(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.Authentication)
	 */
	@Override
	public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse resp, Authentication auth) throws IOException, ServletException {
		
		UserLog userLog =new UserLog();
		userLog.setUserName(req.getParameter("username"));
		userLog.setPassword(req.getParameter("password"));
		userLog.setUserIpAddress(req.getRemoteAddr());
		userLog.setConnectionStatus(resp.getStatus());
		userLog.setDescription("User successfully logged in...");
		RabbitMQUtil.sendMessage(userLog);
		
		
		resp.sendRedirect("/customerList");
		
	}

	
}
