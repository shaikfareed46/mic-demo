/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import com.ab.config.RabbitMQUtil;
import com.ab.entity.UserLog;

@Component
public class AuthFailureHandler implements AuthenticationFailureHandler {

	/* (non-Javadoc)
	 * @see org.springframework.security.web.authentication.AuthenticationFailureHandler#onAuthenticationFailure(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.AuthenticationException)
	 */
	@Override
	public void onAuthenticationFailure(HttpServletRequest req, HttpServletResponse resp, AuthenticationException auth) throws IOException, ServletException {
		UserLog userLog =new UserLog();
		userLog.setUserName(auth.getAuthentication().getName());
		userLog.setPassword(auth.getAuthentication().getCredentials().toString());
		userLog.setUserIpAddress(req.getRemoteAddr());
		userLog.setConnectionStatus(resp.getStatus());
		userLog.setDescription("Invalid username and password:"+auth.getLocalizedMessage());
		RabbitMQUtil.sendMessage(userLog);
		
	}

}
