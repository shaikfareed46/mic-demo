/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ab.entity.Customer;

public interface CustomerRepository extends MongoRepository<Customer, String> {

    /**
     * @param firstName
     * @return
     */
    public Customer findByFirstName(String firstName);

	/**
	 * @param firstName
	 * @param lastName
	 * @return
	 */
	public Customer findByFirstNameAndLastName(String firstName, String lastName);

}