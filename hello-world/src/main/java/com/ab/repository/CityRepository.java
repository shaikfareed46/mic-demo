/**
*
*
* Copyrights of accurate background 
*
* Author : Shaik Fareed
*
* Team: Microservices 
*
**/ 
package com.ab.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.ab.entity.City;

public interface CityRepository extends MongoRepository<City, String> {

}
